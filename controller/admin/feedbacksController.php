<?php

class feedbacksController extends adminController {
	
	function __construct(){
		parent::__construct("Feedback");
	}
  	
	function index(Array $params = []){
    $this->_viewData->hasCreateBtn = true;    

    parent::index($params);
  }  	
}