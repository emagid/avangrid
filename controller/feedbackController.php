<?php

class feedbackController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Feedbacks";
        $this->loadView($this->viewData);
    }

}