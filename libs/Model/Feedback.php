<?php

namespace Model;

class Feedback extends \Emagid\Core\Model {
    public static $tablename = "feedback";

    public static $fields = [
        'rating',
        'comment',
    ];
}