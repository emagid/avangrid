<?php
namespace Model;

use Email\MailMaster;

class Order extends \Emagid\Core\Model {
	static $tablename = "public.order";
    
    public static $status = ['New', 'Paid', 'Payment Declined', 'Shipped', 'Complete', 'Pending Paypal', 'Canceled', 'Imported as Processing', 'Returned'];

	public static $fields  = [
        'insert_time',
        'viewed'=>['type'=>'boolean'],
        // billing info
        'bill_first_name'=>['required'=>true, 'name'=>'Bill - First Name'],
        'bill_last_name'=>['required'=>true, 'name'=>'Bill - Last Name'],
        'bill_address'=>['required'=>true, 'name'=>'Bill - Address'],
        'bill_address2',
        'bill_city'=>['required'=>true, 'name'=>'Bill - City'],
        'bill_state'=>['name'=>'Bill - State'],
        'bill_country'=>['required'=>true, 'name'=>'Bill - Country'],
        'bill_zip'=>['required'=>true, 'type'=>'numeric', 'name'=>'Bill - Zip'],
        //shipping info
        'ship_first_name'=>['required'=>true, 'name'=>'Shipping - First Name'],
        'ship_last_name'=>['required'=>true, 'name'=>'Shipping - Last Name'],
        'ship_address'=>['required'=>true, 'name'=>'Shipping - Address'],
        'ship_address2',
        'ship_city'=>['required'=>true, 'name'=>'Shipping - City'],
        'ship_state'=>['name'=>'Shipping - State'],
        'ship_country'=>['required'=>true, 'name'=>'Shipping - Country'],
        'ship_zip'=>['required'=>true, 'type'=>'numeric', 'name'=>'Shipping - Zip'],
        //card info
        'cc_number'=>['type'=>'numeric', 'name'=>'Credit Card Number'],
        'cc_expiration_month'=>['type'=>'numeric', 'name'=>'Credit Card Expiration Month'],
        'cc_expiration_year'=>['type'=>'numeric', 'name'=>'Credit Card Expiration Year'],
        'cc_ccv'=>['type'=>'numeric', 'name'=>'Credit Card CCV'],
        // price info
        'payment_method'=>['required'=>true, 'type'=>'numeric'], //1 - Credit Card, 2 - Bank Wire
        'shipping_method'=>['required'=>true, 'name'=>'Shipping Method'],
        'shipping_cost'=>['numeric'],
        'subtotal'=>['numeric'],
        'total'=>['numeric'],
        'tax',
        'tax_rate',
        //shipping and tracking info
        'status',
        'tracking_number',
        'user_id',
        'coupon_code',
        'coupon_type',
        'coupon_amount',
        'gift_card',
        'email'=>['type'=>'email'],
        'phone',
        'ref_num' => ['required' => true]
	];

	public function beforeValidate(){
		if ($this->payment_method == 1){
			self::$fields['cc_number']['required'] = true;
			self::$fields['cc_expiration_month']['required'] = true;
			self::$fields['cc_expiration_year']['required'] = true;
		}
		if ($this->bill_country == 'United States'){
			self::$fields['bill_state']['required'] = true;
		}
		if ($this->ship_country == 'United States'){
			self::$fields['ship_state']['required'] = true;
		}
	}

	public static function getDashBoardData($year, $month){
		$data = new \stdClass();

		$sql = " FROM ".self::$tablename." WHERE ";
		$sql .= " EXTRACT(MONTH FROM insert_time)::INTEGER = ".$month;
		$sql .= " AND EXTRACT(YEAR FROM insert_time)::INTEGER = ".$year;
		$sql .= " AND status = '".self::$status[4]."'";

		$data->monthly_sales = 0;
		$monthly_sales = self::execute("SELECT SUM(total) AS s ".$sql);
		if (count($monthly_sales) > 0){
			$data->monthly_sales = $monthly_sales[0]['s'];
		}

		$data->gross_margin = 0;

		$data->monthly_orders = 0;
		$monthly_orders = self::execute("SELECT COUNT(id) AS c ".$sql);
		if (count($monthly_orders) > 0){
			$data->monthly_orders = $monthly_orders[0]['c'];
		}

		return $data;
	}
	
       public static function search($keywords){
        $sql = "select * from public.order where active = 1 and id::varchar like '%".$keywords."%' order by id asc limit 20";
        return self::getList(['sql' => $sql]);
    }

	public function orderProducts()
	{
        $data = [];
		$orderProducts = Order_Product::getList(['where' => ['order_id' => $this->id]]);
//        foreach($orderProducts as $key => $orderProduct){
//            $item = new \stdClass();
//            $item->product = $orderProduct->product();
//            $item->options = $orderProduct->getOptions();
//            $data[] = $item;
//        }
		foreach($orderProducts as $orderProduct){
			$data[] = $orderProduct;
		}
        return $data;
	}

    public function getOrderMergeTags()
    {
        $data = [
            'products' => [],
            'name' => $_POST['ship_first_name'].' '.$_POST['ship_last_name'],
            'lastName' => '',
            'date' => date('F jS, Y', strtotime($this->insert_time))
        ];
        $orderProducts = $this->orderProducts();
        foreach($orderProducts as $orderProduct){
            $product = $orderProduct->product();
            $option = $orderProduct->getOptions();

            $productData = [
                'name' => $product->name,
                'quantity' => $orderProduct->quantity,
                'image' => 'emagidCheckin.com/'.$product->featuredImage(),
                'price' => '$'.number_format($orderProduct->unit_price, 2),
            ];

            $optionProductData = [];
            if($option != null) {
                if (isset($option) && ($optionProduct = $option->getProduct())) {
                    $optionProductData = [
                        'name' => $optionProduct->name,
                        'quantity' => '1',
                        'image' => 'emagidCheckin.com/' . $optionProduct->featuredImage(),
                        'price' => '$' . number_format($optionProduct->price, 2),
                        'size' => $option->getSize() ?: 'None',
                        'shape' => $option->getShape() ?: 'None',
                        'metal' => $option->getMetal() ?: 'None',
                        'color' => $option->getColor() ?: 'None'
                    ];
                } else {
                    $productData = array_merge($productData, [
                        'size' => $option->getSize() ?: 'None',
                        'shape' => $option->getShape() ?: 'None',
                        'metal' => $option->getMetal() ?: 'None',
                        'color' => $option->getColor() ?: 'None'
                    ]);
                }
            }
            $data['products'][] = $productData;
            if($optionProductData){
                $data['products'][] = $optionProductData;
            }
        }

        if($this->user_id){
            $user = User::getItem($this->user_id);
            $data['name'] = $user->full_name();
        }

        $data['billing'] = $this->getFullBillingAddress();
        $data['shipping'] = $this->getFullShippingAddress();
        $data['statusLink'] = 'emagidCheckin.com/status/'.$this->ref_num;
        $data['orderNumber'] = $this->ref_num;
        $data['subtotal'] = '$'.number_format($this->subtotal, 2);
        $data['discount'] = '$'.number_format($this->subtotal * ($this->coupon_amount / 100), 3);
        $data['taxFee'] = '$'.number_format($this->tax, 2);
        $data['shippingFee'] = '$'.number_format(Shipping_Method::getItem($this->shipping_method)->cost, 2);
        $data['total'] = '$'.number_format($this->total, 2);

        return $data;
    }

    public function getFullBillingAddress()
    {
        return "{$this->bill_first_name} {$this->bill_last_name}, {$this->bill_address} {$this->bill_address2}, {$this->bill_city}, {$this->bill_state}, {$this->bill_country} {$this->bill_zip}";
    }

    public function getFullShippingAddress()
    {
        return "{$this->ship_first_name} {$this->ship_last_name}, {$this->ship_address} {$this->ship_address2}, {$this->ship_city}, {$this->ship_state}, {$this->ship_country} {$this->ship_zip}";
    }

    // handler bar json data format

    public function sendOrderSuccessEmail()
    {
        $mailMaster = new \Email\MailMaster();
        $mergeTags = $this->getOrderMergeTags();
        $mailMaster->setTo(['email' => $this->email, 'name' => $mergeTags['name'],  'type' => 'to'])->setMergeTags($mergeTags)->setTemplate('agd-order-success')->setMergeLanguage('handlebars')->send();
    }
}










































