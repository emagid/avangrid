<?php

namespace Model;

class Config extends \Emagid\Core\Model {
 
    static $tablename = "config";

    public static $fields = [
  		'name' => ['required'=>true],
  		'value' => ['required'=>true]
    ];

    public static function getItems(){
    	$list = self::getList();
    	$items = [];
    	foreach($list as $config){
    		$items[$config->name] = $config->value;
    	}
    	return $items;
    }
  
}