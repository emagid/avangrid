<?php
if (count($model->categories) > 0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Order</th>
                <th>In menu?</th>
                <th>Parent</th>
                <th class="text-center">Edit</th>
                <th class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->categories as $obj) { ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>categories/update/<?= $obj->id ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td><?=$obj->display_order?></td>
                    <td><?=$obj->in_menu == 1?'YES':'NO'?></td>
                    <td><?=$obj->parent_category == 0?'None':\Model\Category::getItem($obj->parent_category)->name?></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>categories/update/<?= $obj->id ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?= ADMIN_URL ?>categories/delete/<?= $obj->id ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'categories';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>