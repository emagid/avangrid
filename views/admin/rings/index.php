<div class="row">
    <div class="col-md-16">
        <!--     	<div class="box">
    		<div class="input-group">
				<a href="<?= ADMIN_URL ?>products/google_product_feed" class="btn btn-warning">Google Feed</a>
			</div>
    	</div>	 -->
    </div>
    <div class="col-md-8">
        <div class="box">
            <form action="/admin/products/import" method="post" enctype="multipart/form-data" id="upload">

                <input accept="file/csv" type="file" name="csv" id="im">

                <input type="submit" value="import">

            </form>
            <div class="input-group">
                <input id="search" type="text" name="search" class="form-control" placeholder="Search by Id or Name"/>
            <span class="input-group-addon">
                <i class="icon-search"></i>
            </span>
            </div>
        </div>
    </div>
</div>
<?php if (count($model->rings) > 0): ?>
    <div class="rox">
        <div class="box box-table">
            <table id="data-list" class="table">
                <thead>
                <tr>
                    <th width="5%">Image</th>
                    <th width="40%">Name</th>
                    <th width="8%">14kt Gold</th>
                    <th width="8%">18kt Gold</th>
                    <th width="8%">Platinum</th>
                    <th width="15%" class="text-center">Edit</th>
                    <th width="15%" class="text-center">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($model->rings as $obj){ ?>
                <tr class="originalProducts">
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $obj->id; ?>"><img
                                src="<?php echo $obj->featuredImage(); ?>" width="50"/></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $obj->id; ?>"><?php echo $obj->name; ?></a>
                    </td>
                    <td class="text-right"><a
                            href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $obj->id; ?>"><?= number_format($obj->total_14kt_cost, 2) ?></a>
                    </td>
                    <td class="text-right"><a
                            href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $obj->id; ?>"><?= number_format($obj->total_18kt_cost, 2) ?></a>
                    </td>
                    <td class="text-right"><a
                            href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $obj->id; ?>"><?= number_format($obj->total_plat_cost, 2) ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?php echo ADMIN_URL; ?>rings/update/<?php echo $obj->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>rings/delete/<?php echo $obj->id; ?>?token_id=<?php echo get_token(); ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>

                </tr>
                <?php } ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
                <div class='paginationContent'></div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'rings';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>
<script type="text/javascript">
    $(function () {
        $("#search").keyup(function () {
            var url = "<?php echo ADMIN_URL; ?>products/search";
            var keywords = $(this).val();
            if (keywords.length > 0) {
                $.get(url, {keywords: keywords}, function (data) {
                    $("#data-list tbody tr").not('.originalProducts').remove();
                    $('.paginationContent').hide();
                    $('.originalProducts').hide();

                    var list = JSON.parse(data);

                    for (key in list) {
                        var tr = $('<tr />');
                        if (list[key].featured_image == '') {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=ADMIN_IMG?>itmustbetime_watch.png");
                        } else {
                            var img = $('<img />').prop('width', 50).prop('src', "<?=UPLOAD_URL . 'products/'?>" + list[key].featured_image);
                        }
                        $('<td />').appendTo(tr).html(img);
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].name));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].mpn));
                        $('<td />').appendTo(tr).html($('<a />').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id).html(list[key].price));
                        var editTd = $('<td />').addClass('text-center').appendTo(tr);
                        var editLink = $('<a />').appendTo(editTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>products/update/' + list[key].id);
                        var editIcon = $('<i />').appendTo(editLink).addClass('icon-pencil');
                        var deleteTd = $('<td />').addClass('text-center').appendTo(tr);
                        var deleteLink = $('<a />').appendTo(deleteTd).addClass('btn-actions').prop('href', '<?= ADMIN_URL;?>products/delete/' + list[key].id);
                        deleteLink.click(function () {
                            return confirm('Are You Sure?');
                        });
                        var deleteIcon = $('<i />').appendTo(deleteLink).addClass('icon-cancel-circled');

                        tr.appendTo($("#data-list tbody"));
                    }

                });
            } else {
                $("#data-list tbody tr").not('.originalProducts').remove();
                $('.paginationContent').show();
                $('.originalProducts').show();
            }
        });
    })
</script>
































