<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->feedback->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Ratings</label>
					<?php echo $model->form->textBoxFor("rating"); ?>
				</div>
				<div class="form-group">
					<label>Comments</label>
					<?php echo $model->form->textAreaFor("comment", ["class"=>"ckeditor"]); ?>
				</div>
			</div>
		</div>
		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>

<?= footer(); ?>