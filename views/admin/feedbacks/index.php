<?php if (count($model->feedbacks) > 0){ ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%">No.</th>
                <th width="20%">Ratings</th>
                <th width="50%">Comments</th>
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->feedbacks as $key=>$feedback){ ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>feedbacks/update/<?php echo $feedback->id; ?>"><?php echo $key+1; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>feedbacks/update/<?php echo $feedback->id; ?>"><?php echo $feedback->rating; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>feedbacks/update/<?php echo $feedback->id; ?>"><?php echo $feedback->comment; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>feedbacks/update/<?php echo $feedback->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>feedbacks/delete/<?php echo $feedback->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'feedbacks';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

