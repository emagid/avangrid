<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->about_us->id?>"/>
	<div class="row">
		<div class="col-md-16">
			<div class="box">
				<h4>General</h4>
				<div class="form-group">
					<label>Description</label>
					<?php echo $model->form->textAreaFor("description", ["class"=>"ckeditor"]); ?>
				</div>
			</div>
		</div>
		<div class="col-lg-24">
			<button type="submit" class="btn btn-success btn-lg">Save</button>
		</div>
	</div>
</div>
</form>

<?= footer(); ?>