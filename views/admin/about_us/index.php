<?php if (count($model->about_us) > 0){ ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="5%">Id</th>
                <th width="50%">Description</th>
                <th width="5%" class="text-center">Edit</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($model->about_us as $about){ ?>
                <tr>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about_us/update/<?php echo $about->id; ?>"><?php echo $about->id; ?></a>
                    </td>
                    <td>
                        <a href="<?php echo ADMIN_URL; ?>about_us/update/<?php echo $about->id; ?>"><?php echo $about->description; ?></a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>about_us/update/<?php echo $about->id; ?>">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                    <td class="text-center">
                        <a class="btn-actions"
                           href="<?php echo ADMIN_URL; ?>about_us/delete/<?php echo $about->id; ?>"
                           onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <? } ?>
            </tbody>
        </table>
    </div>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'about_us';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;

</script>

