<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
 
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="test">
        <input type="hidden" name="id" value="<?php echo $model->inquiry->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <?php echo $model->form->editorFor("email"); ?>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <?php echo $model->form->editorFor("phone"); ?>
                    </div>
                    <div class="form-group">
                        <label>Prefer</label>
                        <?php echo $model->form->editorFor("prefer"); ?>
                    </div>
                    <div class="form-group">
                        <label>Message</label>
                        <?php echo $model->form->textAreaFor("message"); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="form-group">
                        <label>Shape</label>
                        <?foreach(\Model\Product::getDiamondShapes() as $short=>$shape){
                            if($model->inquiry->metal_shape == $short ) {
                                $sha = $shape;
                            }}
                            ?>
                        <input type= "text" name="metal_shape" value="<?=$sha?>">
                        <input type= "hidden" name="metal_shape" value="<?=$model->inquiry->metal_shape?>">
                    </div>
                    <div class="form-group">
                        <label>Size</label>
                        <?php echo $model->form->editorFor("size"); ?>
                    </div>
                    <div class="form-group">
                        <label>Color</label>
                        <?php echo $model->form->editorFor("color"); ?>
                    </div>
                    <div class="form-group">
                        <label>Clarity</label>
                        <?php echo $model->form->editorFor("clarity"); ?>
                    </div>
                    <div class="form-group">
                        <label>Budget</label>
                        <?php echo $model->form->editorFor("budget"); ?>
                    </div>

                </div>
            </div>

        </div>
      </div>
    
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 