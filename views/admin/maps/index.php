<?php
if(count($model->maps) > 0) { ?>
	<div class="box box-table">
		<table class="table">
			<thead>
				<tr>
					<th width=5%>Id</th>
					<th width=20%>Map</th>
					<th width=5%>Display Order</th>
					<th width="10%" class="text-center">Edit</th>
          			<th width="10%" class="text-center">Delete</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($model->maps as $obj){  ?>
					<tr>
						<td><a href="<?php echo ADMIN_URL; ?>maps/update/<?php echo $obj->id?>"><?php echo $obj->id;?></a></td>
						<td><a href="<?php echo ADMIN_URL;?>maps/update/<?php echo $obj->id; ?>"><img src="/content/uploads/maps/<?php echo $obj->map_image; ?>" style="max-width: 125px; max-height: 100px; "></a></td>
						<td><a href="<?php echo ADMIN_URL; ?>maps/update/<?php echo $obj->id?>"><?php echo $obj->display_order;?></a></td>
						<td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>maps/update/<?= $obj->id ?>">
				           		<i class="icon-pencil"></i> 
				           	</a>
				        </td>
				        <td class="text-center">
				        	<a class="btn-actions" href="<?= ADMIN_URL ?>maps/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
				           		<i class="icon-cancel-circled"></i> 
				           	</a>
				        </td>
					</tr>
				<? } ?>
			</tbody>
		</table>
	</div>
<? } else { ?>
	<h4>No Maps uploaded yet.</h4>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'maps';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>