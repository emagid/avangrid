<section class='home_page no_scroll'>
    <div class='welcome'>
        <img src="<?=FRONT_ASSETS?>img/logo.png">
    </div>

    <div class='main'>
        <div class='choices'>
            <a href="/home/agenda">
                <img src="<?=FRONT_ASSETS?>img/choice1.png">
                <p>AGENDA</p>
            </a>
            <a href="/home/smartcity">
                <img src="<?=FRONT_ASSETS?>img/choice2.png">
                <p>SMART CITY</p>
            </a>
            <a href="/home/photobooth">
                <img src="<?=FRONT_ASSETS?>img/choice3.png">
                <p>PHOTOBOOTH</p>
            </a>
            <a href="/home/feedback">
                <img src="<?=FRONT_ASSETS?>img/choice4.png">
                <p>FEEDBACK</p>
            </a>
        </div>
    </div>
</section>