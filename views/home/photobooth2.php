<body>
<main>
    <header class="inner_page">
        <a href='/home/photobooth2' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo.png"></a>
<!--             <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
        <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
    </header> 

  <div class='background'></div> 

  <section class="home photobooth" >
        <!-- home button -->
        <a href="//photobooth2"><aside id='home_click_white' class='home_click'>
            <img class='white_img' src="<?=FRONT_ASSETS?>img/home.png"> 
        </aside></a>

    <body>
      <!--  ==========  PHOTOS  =============== -->
      <section id='photos' class='photos'>

        <!-- Cam -->
        <h3 class='pic_text'>Click below and look up!</h3>
        <video id="video" width="1080px" height="1700px" autoplay></video>
                <device type="media" onchange="update(this.data)"></device>

                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

          <div id='snap_photo' class='camera_button'>         
            <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
          </div>

          <div id='snap_gif' class='camera_button'>                 
              <img src="<?=FRONT_ASSETS?>img/gif_cam.png">
          </div>

          <!-- countdown --><div class='countdown'>3</div>
          <!-- countdown --><div class='countdown'>2</div>
          <!-- countdown --><div class='countdown'>1</div>
          <!-- flash --><div class='flash'></div>
      </section>

      <!-- Choosing pictures -->
      <div id='pictures'>
                <!-- <img id='pen' src="<?= FRONT_ASSETS ?>img/draw.png"> -->

                <div class='draw_ctl drawing'>
                    <i class="fa fa-close" style="font-size:36px"></i>
                </div>

                <div class='colors drawing'>
                    <div class='color' id='colorRed'></div>
                    <div class='color' id='colorGreen'></div>
                    <div class='color' id='colorBlue'></div>
                    <div class='color' id='colorWhite'></div>
                    <div class='color' id='colorBlack'></div>
                </div>


                <div class='sizes drawing'>
                    <div class='size' id='small'></div>
                    <div class='size' id='medium'></div>
                    <div class='size' id='large'></div>
                    <div class='size' id='huge'></div>
                </div>

                 <!-- <div  id='canvasDiv'></div> -->


        <div class='button next'>NEXT</div>
        <p class='gif_info'>Choose 4 photos to create your GIF</p>
        <div class='button submit'>SHARE</div>
        <a href="/home/photobooth"><div class='button retake'>RETAKE</div></a>
        <div class='container gif_container'></div>
      </div>

      <!-- Showing Gif -->
      <section class='gif_show'>
                <img id='loader' src="<?=FRONT_ASSETS?>img/loader.gif">
        <div class='button submit'>SHARE</div>
      </section>

      <!-- Share overlay -->
      <i class="fa fa-close sharex" style="font-size:36px"></i>
      <section class='share_overlay'>
        <div class='container'>
          <form id='submit_form'>
            <input type='hidden' name='form' value="1">
                        <input type="hidden" name="image" class="image_encoded">
                        <span>
                            <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
                        </span>
                        <p id='add_email' style='color: #ffffffab;'>Add an email +</p>
                        <div class='line'></div>
                        <span>
                            <input class='input jQKeyboard first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
                        </span>
                        <p id='add_phone' style='color: #ffffffab;'>Add a phone number +</p>
            <input id='share_btn' class='button' type='submit' value='SHARE'>
                        <!-- <div class='button print'>PRINT</div> -->
          </form>
          <!-- <a href='/' id='done' class='button'>DONE</a> -->
        </div>
      </section>

      <!-- Alerts -->
      <section id='share_alert'>
                <h3>Thank you!</h3>
        <p>Your picture is on it's way</p>
      </section>

    </body>

        <section class='event_pics'>
        <div class='overlay'>
          <div class='bubble'>
            <img src="<?=FRONT_ASSETS?>img/event_pics.png">
            <p>EVENT PICTURES</p>
          </div>
            <!--  -->
            <div class='inner_content'>
                <div id='event_images' class='button'>IMAGES</div>
                <div id='event_gifs' class='button'>GIFS</div>
                <div class='display_images'><p class='top'><i class="arrow up"></i></p></div>
                <div class='display_gifs'><p class='top'><i class="arrow up"></i></p></div>
            </div>
        </div>
    </section>

  </section>

</main>
</body>
 <!-- <script type="text/javascript" src="<?= FRONT_JS ?>html5-canvas-drawing-app.js"></script>
    <script type="text/javascript">
         drawingApp.init();
    </script> -->

            
<script type="text/javascript">
    $(document).ready(function(){
    // alert('hi')
       
        var images = <?= \Model\Snapshot_Contact::slider()?>; //array of image urls
        var gifs =   <?= \Model\Gif::slider()?>; //array of gif urls

        addMedia(images, gifs);


        $('#add_email').click(function(){
            $('.first_email').parent().append("<input required class='input jQKeyboard' name='email[]' pattern='[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$' type='text' placeholder='Email' title='Please enter a valid email address.'>");
            $('.first_email').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#add_phone').click(function(){
            $('.first_phone').parent().append("<input required class='input jQKeyboard' name='phone[]' placeholder='Phone Number (eg: +19874563210)'>");
            $('.first_phone').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#video').fadeIn(1000);

        setTimeout(function(){
            $('.pic_text').fadeIn(2000);
            $('#snap_photo').fadeIn(2000);
            $('#snap_gif').fadeIn(2000);
        }, 1000);

        setTimeout(function(){
            $('#video').fadeIn(1000);
        }, 2000);


        function addMedia(images, gifs) {

        for ( i=0; i<images.length; i++ ) {
          var div = "<img class='event_pic' src='" + images[i].replace('\\','/') + "'></div>";
          $('.event_pics .display_images').append(div);
        }
            
        for ( i=0; i<gifs.length; i++ ) {
              var div = "<img class='event_pic' src='" + gifs[i].replace('\\','/') + "'></div>";
            $('.event_pics .display_gifs').append(div);
        }
      }


      // ===============================================  PHOTOBOOTH CODE  ===============================================

         // Checkmark
      var count = (function (num) {
          var counter = 0;
          return function (num) {return counter += num;}
      })();
      

       $(document).on('click', '.gif', function(){
          var num = count(1);
          if ( !$(this).hasClass('checked') && num < 5 ) {            
              if ( num <= 4  ) {
                  $(this).css('outline', '#45b848 solid 5px')
                  $(this).addClass('checked');
                  if ( num  === 4 ) {
                      $('.gif_info').fadeOut();
                      $('.next').slideDown();
                  }
              }

          }else if ( $(this).hasClass('checked') ) {
              num = count(-2);
              $(this).removeClass('checked');
              $(this).css('outline', '0px')
              if ( num  === 4 ) {
                      $('.next').slideDown();
                      $('.gif_info').fadeOut();

              }else {
                  $('.next').slideUp();
                  $('.gif_info').fadeIn();

              }

          }else {
              num = count(-1);
          }
       });




       // Gif Next click
       function picAction( pic ) {
        $(pic).fadeIn('fast');
        $(pic).delay(1300).fadeOut(400);
      }

       function gif( pics ) {
        var offset = 0

        for ( i=0; i<2; i++ ) {
          pics.each(function(){
            var timer
            var self = this;
            timer = setTimeout(function(){
                picAction(self)
            }, 0 + offset);

            offset += 1500;
          });
        }

        setTimeout(function(){
              var gif_pics = $('.gif_show').children('.checked');
              gif( gif_pics )
          }, 1200);
      }

       $(document).on('click', '.next', function(){
          $('#pictures').fadeOut();
          $('.gif_show').fadeIn();
          $('.gif_show').addClass('flex');

          // =========  NEED TO TURN IMAGES INTO GIF TO SHOW HERE  ================
          $('#submit_form').find('.image_encoded').remove();
          $('.canvas_holder.checked img').each(function () {
              $('#submit_form').append($('<input type="hidden" name="images[]" value="'+$(this).attr('src')+'">'));
          });
          let deferreds = [];
          let imgs = [];
          $("#submit_form input[name='images[]']").each(function(i,el){
              deferreds.push(
                  $.post('/contact/make_frame/', {
                      image: el.value
                  },function(data) {
                    // Need to manually type in the url
                      var newref = "https://cognizant.popshap.net/"
                      imgs.push(newref+'content/uploads/Snapshots/'+data.image.image);
                      $('input[name="images[]"]')[i].value = data.image.id;
                  }));
          });

          $('.fa-close').click(function(){
            $('.share_overlay').slideUp();
            $('.jQKeyboardContainer').slideUp();
            $(this).fadeOut();
            });


          //Show loading image
          $.when(...deferreds).then( function() {
               gifshot.createGIF({
                   'images': imgs,
                   'frameDuration':4,
                   'gifWidth': 900,
                   'gifHeight': 675
               },function(obj) {
                   if(!obj.error) {
                       var image = obj.image;
                       console.log(image)
                       $('#loader').fadeOut();
                       $('.button.submit').fadeIn();
                       $('.gif_show').append("<img src=' " + image + "'>")
                       $('#submit_form').append($('<input type="hidden" name="gif" value="'+image+'">'));
                       $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
                           $('#submit_form input[name=gif]').val(data.gif.id);
                       });
                   }
               });
           });


       });


       $('.event_pics .overlay').on('click', function(){
        $('.photos').css('opacity', '0');


        $(this).children('h2').fadeOut(1500);

        setTimeout(function(){
            $('.inner_content').css('display', 'flex');
            $('.event_pics').css('height', '1920px');
            $('.event_pics').addClass('active');

            $('.photos').hide();
            $('.inner_content').slideDown(200);
            // $('.display_images').slideDown();
            $('.choice').css('opacity', '1');
            $('.bubble').hide();

        }, 1000);

        setTimeout(function(){
            $('#home_click').fadeIn();
            $('.event_pics').css('min-height', '1920px');
            $('.event_pics').css('touch-events', 'none');
        }, 1500);
    });
      


    // click of showing images
     $('.event_images').on('click', function(){
        $('.button').fadeOut();
        $('#event_gifs').fadeIn();
        $('.display_images').slideDown();
     });


      // click of showing gifs
     $('.event_gifs').on('click', function(){
        $('.button').fadeOut();
        $('#event_images').fadeIn();
        $('.display_gifs').slideDown();
     });


      // click of showing gifs when images are shown
     $('#event_gifs').on('click', function(){
        $('#event_images').fadeIn();
        $('.button').fadeOut();
        $('#event_gifs').fadeOut();
        $('.display_images').slideUp();
        setTimeout(function(){
            $('#event_images').fadeIn();
            $('#event_images').addClass('shrunk');
            $('.display_gifs').slideDown();
        }, 1000)
     });

     // click of showing images when gifs are shown
     $('#event_images').on('click', function(){
        $('.button').fadeOut();
        $('.display_gifs').slideUp();
        setTimeout(function(){
            $('#event_gifs').fadeIn();
            $('#event_gifs').addClass('shrunk');
            $('.display_images').slideDown();
        }, 1000)
     });



       // Share click
       $(document).on('click', '.submit', function(){
         
          if ( $('#draw').is(':visible') ) {
               saveDrawing();


              $.post('/contact/save_img/',{image:$('#pictures > img:last-child').attr('src')},function(data){
                  $('#submit_form input[name=image]').val(data.image.id);
              });
          }

          $('.share_overlay').slideDown();
          $('.fa-close').fadeIn();
       });

        $('#share_btn').click(function(){
          $('.fa-close').fadeOut();
          $('.jQKeyboardContainer').fadeOut();
       })

       function saveDrawing() {
              
              var draw_canvas = $('#draw');
              $($(draw_canvas[0]).parents('#pictures')[0]).append(convertCanvasToImage(draw_canvas[0]))

              $('.drawing').hide();
          }
       


       // Form submit
       $('#submit_form').on('submit', function(e){
          e.preventDefault();
          $('#share_alert').slideDown();
          $('#share_alert').css('display', 'flex');
          $.post('/contact', $(this).serialize(), function (response) {
              if(response.status){
                  $('#submit_form')[0].reset();
                  window.location.href = '/photobooth2';
              }
          });
       });


   $('.fa-close').click(function(){
        $('.share_overlay').slideUp();
        $('#jQKeyboardContainer').slideUp();
        $('.fa-close').fadeOut();
    });


       // Click of done sharing
       $('#done').on('click', function(e){
          $('.share_overlay').slideUp();
          $('#jQKeyboardContainer').remove()
          // remove all images
          $('#pictures').children('.canvas_holder');
       });


       $('.print').click(function(){
            saveDrawing();  
            var source = $($('#pictures img:last-child')[1]).attr('src');
            VoucherPrint(source);
       });


       function convertCanvasToImage(canvas) {
          var image = new Image();
          image.src = canvas.toDataURL("image/png");
          console.log(image)
          return image;
      }





      // ===========  CAMERA FUNCTION  ==============

    // Grab elements, create settings, etc.
    var video = document.getElementById('video');

    // Get access to the camera!
    if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

 
    function countDown() {
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '150px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });

        // flash
        setTimeout(function(){
            $('.flash').fadeIn(200);
            $('.flash').fadeOut(400);
            $(divs).css('font-size', '0px');
        }, 4000);
    }


    function gifPhotos() {
        var divs = $('.countdown');
        var offset = 1000

        for ( i=0; i<5; i++) {

            (function(i){
              setTimeout(function(){
                    $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")
                    var canvas = $('.canvas');
                    var context = canvas[canvas.length -1].getContext('2d');
                    var video = document.getElementById('video');
                    var background = $('#background')[0];

                     $('.flash').fadeIn(200);
                    $('.flash').fadeOut(400);

                    $(divs).css('font-size', '0px');

                   context.drawImage(video, 0, 0, 900, 675);
            
                   $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');

                   // watermark
                    // var img2 = new Image();
                    // img2.src = '/content/frontend/assets/img/logo_holder.png';
                    // context.drawImage(img2,530,420, 80, 43);

                    $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
              
              }, offset);
              offset += 1000
            }(i));  
        }
    }


    function showGifs() {
        var photos = $('#pictures').children('.canvas_holder');
        $('#photos').hide();
        $('#pictures').fadeIn(1000);
        $('.gif_info').fadeIn();
        $('.home').css('position', 'relative');
        // debugger
        for ( i=0; i<6; i++ ) {
            $(photos[i]).css('display', 'inline-block');
        }
    }


    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function convertCanvasToImage2(canvas) {
        var dataURL = canvas.toDataURL();
        return canvas.src = dataURL;
    }

    


    function showPictures() {
        $('#pictures').fadeIn(2000);
        var photos = $('#pictures').children('.photo');
            $(photos[0]).fadeIn();

        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });

        $('#pen').fadeIn();

    }



    // GIF CLICK
    document.getElementById("snap_gif").addEventListener("click", function() {
        $('.camera_button').fadeOut(1000);
        $('.camera_button').css('pointer-events', 'none');
        var pics = []

        countDown();
        setTimeout(function(){
            $('#pictures .gif_container').append("<div class='canvas_holder gif'><canvas class='canvas' width='900' height='675'></canvas></div>")                
                var canvas = $('.canvas');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               context.drawImage(video, 0, 0, 900, 675);
               

               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).css('display', 'inline-block');
               $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


           gifPhotos();

        }, 4000 )

        setTimeout(function(){
            $('#photos').css('opacity', '0');
            setTimeout(function(){
                $('#photos').fadeOut();
            }, 1100);

            showGifs();
            $(this).css('pointer-events', 'all');
        }, 10000);
    });



    // PHOTO CLICK
    document.getElementById("snap_photo").addEventListener("click", function() {
        $('.camera_button').fadeOut(1000);
        $('.camera_button').css('pointer-events', 'none');
        var pics = []

        countDown();
            // take the picture
            setTimeout(function(){
                $('#pictures').append("<div class='canvas_holder photo'><canvas id='pic' class='canvas' width='900' height='675'></canvas></div>")
                
                var canvas = $('#pic');
                var context = canvas[canvas.length -1].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               context.drawImage(video, 0, 0, 900, 675);
               

                $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))


                $('#pictures').append("<canvas id='draw' class='drawing' width='900' height='675'></canvas>");
                var drawcanvas = $('#draw');
                var ctx = drawcanvas[0].getContext('2d');
                var video = document.getElementById('video');
                var background = $('#background')[0];
               ctx.drawImage(video, 0, 0, 900, 675);
               // ctx.drawImage(background, 0, 0, 900, 675);






            }, 4000);

        setTimeout(function(){
            $('#photos').slideUp()
            $('.submit, .retake').delay(1000).fadeIn();
            $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 5000);
    });



    // DRAWING
    $('#pen').click(function(){
        // $('.canvas_holder').append("<canvas id='draw' width='900' height='675'></canvas>")
       context = document.getElementById('draw').getContext("2d");
        $('#draw').fadeIn()
        $('.draw_ctl').fadeIn();
        $('.colors').fadeIn();
        $('.sizes').fadeIn();
        $('#pictures .canvas_holder img').hide();

        can = document.getElementById('draw');
        can.addEventListener('mousedown', onMouseDown);
        can.addEventListener('touchstart', onTouchStart);
        can.addEventListener('mousemove', onMouseMove);
        can.addEventListener('touchmove', onTouchMove);
        can.addEventListener('mouseup', onMouseEnd);
        can.addEventListener('touchend', onMouseEnd);
        can.addEventListener('mouseleave', onMouseEnd);
        can.addEventListener('touchleave', onMouseEnd);
        $(this).fadeOut();
    });



    function onMouseDown (e){

      var mouseX = e.pageX - this.offsetLeft;
      var mouseY = e.pageY - this.offsetTop;
      console.log(e)

            
      paint = true;
      addClick(e.clientX - this.offsetLeft, e.clientY - this.offsetTop);
      redraw();
      onTouchMove(e);
    };

    function onTouchStart (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop);
        redraw();
      
    };

    function onMouseMove (e){
        if(paint){
            addClick(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
            redraw();
            onTouchMove(e)
      }
      
    };

    function onTouchMove (e){
        addClick(e.touches[0].screenX - this.offsetLeft, e.touches[0].screenY - this.offsetTop, true);
        redraw();
    };

    function onMouseEnd (e){
         paint = false;
    };



    var clickX = new Array();
    var clickY = new Array();
    var clickDrag = new Array();
    var paint;
    var colorWhite = "#ffffff";
    var colorGreen = "#49a463";
    var colorBlue = "#30b9dc";
    var colorBlack = "#000000";
    var colorRed = "#c22628";

    var curColor = colorRed;
    var clickColor = new Array();

    $('#colorWhite').click(function(){
        curColor = colorWhite
    });

    $('#colorGreen').click(function(){
        curColor = colorGreen
    });

    $('#colorBlue').click(function(){
        curColor = colorBlue
    });

    $('#colorBlack').click(function(){
        curColor = colorBlack
    });

    $('#colorRed').click(function(){
        curColor = colorRed
    });

    $('.color').click(function(){
        $('.canvas_holder').css('background-color', curColor);
        setTimeout(function(){
            $('.canvas_holder').css('background-color', 'transparent');
        }, 200)
    });


    var small = 5;
    var medium = 13;
    var large = 20;
    var huge = 30;

    var curSize = medium;
    var clickSize = new Array();

    $('#small').click(function(){
        curSize = small;
    });

    $('#medium').click(function(){
        curSize = medium;
    });

    $('#large').click(function(){
        curSize = large;
    });

    $('#huge').click(function(){
        curSize = huge;
    });



    

    function addClick(x, y, dragging)
    {
      clickX.push(x);
      clickY.push(y);
      clickDrag.push(dragging);
      clickColor.push(curColor);
      clickSize.push(curSize);
    }

    function redraw(){
      // context.clearRect(0, 0, context.canvas.width, context.canvas.height); // Clears the canvas
      
      context.strokeStyle = "#df4b26";
      context.lineJoin = "round";
      

                
      for(var i=0; i < clickX.length; i++) {        
        context.beginPath();
        if(clickDrag[i] && i){
          context.moveTo(clickX[i-1], clickY[i-1]);
         }else{
           context.moveTo(clickX[i]-1, clickY[i]);
         }
         context.lineTo(clickX[i], clickY[i]);
         context.closePath();
         context.strokeStyle = clickColor[i];
         context.lineWidth = clickSize[i];
         context.stroke();
      }
    }


    $('.draw_ctl').click(function(){
      clickX = new Array();
      clickY = new Array();
      clickDrag = new Array();
      clickColor = new Array();
      clickSize = new Array();
      context.clearRect(0, 0, 900, 675);

      var drawcanvas = $('#draw');
      var ctx = drawcanvas[0].getContext('2d');
      var image = $('#pictures .canvas_holder img')[0];
       ctx.drawImage(image, 0, 0, 900, 675);
    });
            
    });
</script>


<script type="text/javascript">
    var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.donate_key').initKeypad({'donateKeyboardLayout': board});
            });
  </script>

  <link rel="stylesheet" type="text/css" href="<?=FRONT_CSS?>jQKeyboard.css">

  <script>
      (function($){

    var keyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ],
                [
                    ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                    ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                    ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                    ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                    ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                    ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                    ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                ]
            ]
        ]
    };


        var donateKeyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                ]
            ]
        ]
    };

    var activeInput = {
        'htmlElem': '',
        'initValue': '',
        'keyboardLayout': keyboardLayout,
        'donateKeyboardLayout': donateKeyboardLayout,
        'keyboardType': '0',
        'keyboardSet': 0,
        'dataType': 'string',
        'isMoney': false,
        'thousandsSep': ',',
        'disableKeyboardKey': false
    };

    /*
     * initialize keyboard
     * @param {type} settings
     */
    $.fn.initKeypad = function(settings){
        //$.extend(activeInput, settings);

        $(this).click(function(e){
            $("#jQKeyboardContainer").remove();
            activateKeypad(e.target);
        });
    };
    
    /*
     * create keyboard container and keyboard button
     * @param {DOM object} targetInput
     */
    function activateKeypad(targetInput){
        if($('div.jQKeyboardContainer').length === 0)
        {
            activeInput.htmlElem = $(targetInput);
            activeInput.initValue = $(targetInput).val();

            $(activeInput.htmlElem).addClass('focus');
            createKeypadContainer();
            createKeypad(0);
        }
    }
    
    /*
     * create keyboard container
     */
    function createKeypadContainer(){
        var container = document.createElement('div');
        container.setAttribute('class', 'jQKeyboardContainer');
        container.setAttribute('id', 'jQKeyboardContainer');
        container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
        
        $('body').append(container);
    }
    
    /*
     * create keyboard
     * @param {Number} set
     */
    function createKeypad(set){
        $('#jQKeyboardContainer').empty();
        if ( $('input.donateinput').is(':focus') ) {
            var layout = activeInput.donateKeyboardLayout.layout[activeInput.keyboardType][set];
        }else {
            var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
        }
        

        for(var i = 0; i < layout.length; i++){

            if(layout[i][4]){
                var row = document.createElement('div');
                row.setAttribute('class', 'jQKeyboardRow');
                row.setAttribute('name', 'jQKeyboardRow');
                $('#jQKeyboardContainer').append(row);
            }

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', 'key' + layout[i][2]);
            button.setAttribute('id', 'key' + layout[i][2]);
            button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
            button.setAttribute('data-text', layout[i][0]);
            button.setAttribute('data-value', layout[i][1]);
            button.innerHTML = layout[i][0];
            
            $(button).click(function(e){
               getKeyPressedValue(e.target); 
            });

            $(row).append(button);
        }
    }
    /*
     * remove keyboard from kepad container
     */
    function removeKeypad(){
        $('#jQKeyboardContainer').remove();
        $(activeInput.htmlElem).removeClass('focus');
    }
    
    /*
     * handle key pressed
     * @param {DOM object} clickedBtn
     */
    function getKeyPressedValue(clickedBtn){
        var caretPos = getCaretPosition(activeInput.htmlElem);
        var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
        
        var currentValue = $(activeInput.htmlElem).val();
        var newVal = currentValue;
        var closeKeypad = false;
        
        /*
         * TODO
        if(activeInput.isMoney && activeInput.thousandsSep !== ''){
            stripMoney(currentValue, activeInput.thousandsSep);
        }
        */
        
        switch(keyCode){
            case 8:     // backspace key
                newVal = onDeleteKeyPressed(currentValue, caretPos);
              caretPos--;
                break;
            case 13:    // enter key
                closeKeypad = onEnterKeyPressed();
                break;
            case 16:    // shift key
                onShiftKeyPressed();
                break;
            case 27:    // cancel key
                closeKeypad = true;
                newVal = onCancelKeyPressed(activeInput.initValue);
                break;
            case 32:    // space key
                newVal = onSpaceKeyPressed(currentValue, caretPos);
                caretPos++;
    break;
            case 46:    // clear key
                newVal = onClearKeyPressed();
                break;
            case 190:   // dot key
                newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
            default:    // alpha or numeric key
                newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
        }
        
        // update new value and set caret position
        $(activeInput.htmlElem).val(newVal);
        setCaretPosition(activeInput.htmlElem, caretPos);

        if(closeKeypad){
            removeKeypad();
            $(activeInput.htmlElem).blur();
        }
    }
    
    /*
     * handle delete key pressed
     * @param value 
     * @param inputType
     */
    function onDeleteKeyPressed(value, caretPos){
        var result = value.split('');
        
        if(result.length > 1){
            result.splice((caretPos - 1), 1);
            return result.join('');
        }
    }
    
    /*
     * handle shift key pressed
     * update keyboard layout and shift key color according to current keyboard set
     */
    function onShiftKeyPressed(){
        var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
        activeInput.keyboardSet = keyboardSet;

        createKeypad(keyboardSet);
        
        if(keyboardSet === 1){
            $('button[name="key16"').addClass('shift-active');
        }else{
            $('button[name="key16"').removeClass('shift-active');
        }
    }
    
    /*
     * handle space key pressed
     * add a space to current value
     * @param {String} curVal
     * @returns {String}
     */
    function onSpaceKeyPressed(currentValue, caretPos){
        return insertValueToString(currentValue, ' ', caretPos);
    }
    
    /*
     * handle cancel key pressed
     * revert to original value and close key pad
     * @param {String} initValue
     * @returns {String}
     */
    function onCancelKeyPressed(initValue){
        return initValue;
    }
    
    /*
     * handle enter key pressed value
     * TODO: need to check min max value
     * @returns {Boolean}
     */
    function onEnterKeyPressed(){
        return true;
    }
    
    /*
     * handle clear key pressed
     * clear text field value
     * @returns {String}
     */
    function onClearKeyPressed(){
        return '';
    }
    
    /*
     * handle dot key pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onDotKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * handle all alpha numeric keys pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * insert new value to a string at specified position
     * @param {String} currentValue
     * @param {String} newValue
     * @param {Number} pos
     * @returns {String}
     */
    function insertValueToString(currentValue, newValue, pos){
        var result = currentValue.split('');
        result.splice(pos, 0, newValue);
        
        return result.join('');
    }
    
   /*
    * get caret position
    * @param {DOM object} elem
    * @return {Number}
    */
    function getCaretPosition(elem){
        var input = $(elem).get(0);

        if('selectionStart' in input){    // Standard-compliant browsers
            return input.selectionStart;
        } else if(document.selection){    // IE
            input.focus();
            
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
    
    /*
     * set caret position
     * @param {DOM object} elem
     * @param {Number} pos
     */
    function setCaretPosition(elem, pos){
        var input = $(elem).get(0);
        
        if(input !== null) {
            if(input.createTextRange){
                var range = elem.createTextRange();
                range.move('character', pos);
                range.select();
            }else{
                input.focus();
                input.setSelectionRange(pos, pos);
            }
        }
    }
})(jQuery);




</script> 


