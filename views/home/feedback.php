<style type="text/css">
  div.jQKeyboardContainer {
    padding-top: 135px;
  }

  .sharex {
    position: absolute;
    top: 39px;
    left: 50%;
    transform: translateX(-50%);
    width: fit-content;
  }
</style>


<section class='feedback_page no_scroll'>
    <div class='intro'>
        <p class='title'>Feedback</p>
        <p>Please rate the event and feel free to leave a comment</p>
    </div>

    <div class='content grey'>
        <form class='feedback'>
          <div class='stars'>
            <i id='1' class="fas fa-star"></i>
            <i id='2' class="fas fa-star"></i>
            <i id='3' class="fas fa-star"></i>
            <i id='4' class="fas fa-star"></i>
            <i  id='5' class="fas fa-star"></i>
          </div>
          <input id='rating' type="hidden" name="rating" value='5'>
          <textarea id="comment" class='jQKeyboard' type="textarea" name="comments" placeholder="Comments"></textarea>
          <input class='button' type="submit" value="SUBMIT">
        </form>
    </div>
    <i class="fa fa-close sharex" style="font-size:36px"></i>
    <section id='share_alert'>
        <h3>Thank you!</h3>
        <p>We appreciate the feedback!</p>
      </section>
</section>


<script type="text/javascript">

    $('.stars i').click(function(){
      var id = $(this).attr('id');
      $('.stars i').removeClass('fas');
      $('.stars i').addClass('far');
      // console.log(id);
      for(var i = 0; i < parseInt(id); i++) {
          $('#' + (parseInt(id) - i)).addClass('fas');
      }
      $('#rating').val(id);
    });

    $('.jQKeyboard').click(function(){
      $('.sharex').fadeIn();
    });

    $('.sharex').click(function(){
      $('.jQKeyboardContainer').slideUp();
      $('.sharex').hide();
    });

    $('.feedback').submit(function(e){
      e.preventDefault();
      $('.jQKeyboardContainer').hide();
      $('.sharex').hide();
      var rating = $('#rating').val();
      var comment = $('#comment').val();
      $.post("/home/feedback",{rating: rating, comment: comment}, function(response){
        // console.log(response);
        if(response.status){
          $('#share_alert').slideDown();
          $('#share_alert').css('display', 'flex');
          $('.stars i').css('opacity', '0');

          setTimeout(function(){
            $('.white').fadeIn();
            setTimeout(function(){
              window.location = '/';
            }, 1000)
          }, 3000);
        }
      }) 
      // $('#share_alert').slideDown();
      // $('#share_alert').css('display', 'flex');
      // $('.stars i').css('opacity', '0');

      // setTimeout(function(){
      //   $('.white').fadeIn();
      //   setTimeout(function(){
      //     window.location = '/';
      //   }, 1000)
      // }, 3000);
    });

    // $('.teams').masonry({
    //   itemSelector: '.team img'
    // });
    // $('.teams').imagesLoaded().progress( function() {
    //   $('.teams').masonry();
    // });  
</script>