<section class='smartcity_page'>
    <div class='intro'>
        <p class='title'>Smart City 2018 Teams</p>
        <p>Click the info icon <i class="fas fa-info"></i> to learn more</p>
    </div>

    <div class='content grey'>
        <div class='teams'>
           <div class='team' >
            <div class='holder'>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team1_2.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team1_3.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team1_4.jpg')"></div>
              <div class='person' style="width: 94%;  background-image: url('<?= FRONT_ASSETS ?>img/team1_1.jpg')"></div>
            </div>
            <div class='banner' style='background-color: rgba(165,221,80,.8)'>
                <p>Ville Verde</p>
                <i class="fas fa-info"></i>
            </div>
            <div class='more_info' style='background-color: rgba(165,221,80,.95)'>
              <p class='title'>Ville Verde</p>
              <p>Utility-as-a-service - eEnergy procurement through green tariffs. RECs, IRPs, electrification of transportation and retail choice. Incentivize service bundles.</p>
              <i class="fas fa-times"></i>
            </div>
           </div>

           <div class='team' >
            <div class='holder'>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team2_1.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team2_2.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team2_3.jpg')"></div>
              <div class='person' style="width: 46%; background-image: url('<?= FRONT_ASSETS ?>img/team2_4.jpg')"></div>
              <div class='person' style="width: 46%; background-image: url('<?= FRONT_ASSETS ?>img/team2_5.jpg')"></div>
            </div>
            <div class='banner' style='background-color: rgba(0,112, 192,.8)'>
                <p>Future D.R.eam</p>
                <i class="fas fa-info"></i>
            </div>
            <div class='more_info' style='background-color: rgba(0,112, 192,.95)'>
              <p class='title'>Future D.R.eam</p>
              <p>Peer-to-peer marketplace for excess distributed energy generation using smart meter data to ID real-time usage and generation.</p>
              <i class="fas fa-times"></i>
            </div>
           </div>

           <div class='team'>
            <div class='holder'>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team3_1.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team3_2.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team3_3.jpg')"></div>
              <div class='person' style="width: 46%; background-image: url('<?= FRONT_ASSETS ?>img/team3_4.jpg')"></div>
              <div class='person' style="width: 46%; background-image: url('<?= FRONT_ASSETS ?>img/team3_5.jpg')"></div>
            </div>
            <div class='banner' style='background-color: rgba(166,166,166,.8)'>
                <p>Charge Up Portland</p>
                <i class="fas fa-info"></i>
            </div>
           <div class='more_info' style='background-color: rgba(166,166,166,.95)'>
              <p class='title'>Charge Up Portland</p>
              <p>Reimagining Transit. Leveraging public-private partnerships to deploy electric buses and EV charging to alleviate traffic, minimize grid congestion, and transition Portland to a healthy, multi-modal city in line with Portland’s 2030 Vision Plan.</p>
              <i class="fas fa-times"></i>
            </div>
           </div>

           <div class='team'>
            <div class='holder'>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team4_1.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team4_2.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team4_3.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team4_4.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team4_5.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team4_6.jpg')"></div>
            </div>
            <div class='banner' style='background-color: rgba(92,136,26,.8)'>
                <p>NEOS</p>
                <i class="fas fa-info"></i>
            </div>
           <div class='more_info' style='background-color: rgba(92,136,26,.95)'>
              <p class='title'>NEOS</p>
              <p><strong>Orchestration of peak demand management</strong> through an intelligent hub that leverages smart meter data and analytics to create a seamless user experience</p>
              <i class="fas fa-times"></i>
            </div>
           </div>

           <div class='team'>
            <div class='holder'>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team5_1.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team5_2.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team5_3.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team5_4.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team5_5.jpg')"></div>
              <div class='person' style="background-image: url('<?= FRONT_ASSETS ?>img/team5_6.jpg')"></div>
            </div>
            <div class='banner' style='background-color: rgba(255,90,0,.8)'>
                <p>Smart Energy Solutions</p>
                <i class="fas fa-info"></i>
            </div>
            <div class='more_info' style='background-color: rgba(255,90,0,.95)'>
              <p class='title'>Smart Energy Solutions</p>
              <p>Community Energy Storage Model - Utility managed front-of-the-meter. Key locations coupled with transportation electrification. Also couple with future Transmission Proposals.</p>
              <i class="fas fa-times"></i>
            </div>
           </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  $('.teams').slick({
    arrows: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.smartcity_page p .fa-info').click(function(){
    var self = $('.slick-active .fa-info');
    $(self).parents('.team').children('.more_info').fadeIn(500);
    $(self).parents('.team').children('.more_info').css('display', 'flex');
    $('.slick-next, .slick-prev').hide();
    $('.teams').slick('slickPause');
  });


  $('.fa-info').click(function(){
      $(this).parents('.team').children('.more_info').fadeIn(500);
      $(this).parents('.team').children('.more_info').css('display', 'flex');
      $('.slick-next, .slick-prev').hide();
      $('.teams').slick('slickPause');
  });

    $('.fa-times').click(function(){
      $('.more_info').fadeOut(500);
      $('.slick-next, .slick-prev').show();
      $('.teams').slick('slickPlay');
  });

    // $('.teams').masonry({
    //   itemSelector: '.team img'
    // });
    // $('.teams').imagesLoaded().progress( function() {
    //   $('.teams').masonry();
    // });  
</script>